package pl.mardom92.Log.Service.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import pl.mardom92.Log.Service.dto.LogDtoInput;
import pl.mardom92.Log.Service.mapper.LogMapper;
import pl.mardom92.Log.Service.model.Log;
import pl.mardom92.Log.Service.repository.LogRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumerService {

    private static final LogMapper logMapper = LogMapper.INSTANCE;
    private final LogRepository logRepository;

    @KafkaListener(
            topics = "#{'${spring.kafka.consumer.topics}'.split(',')}",
            groupId = "${spring.kafka.consumer.group-id}")
    public void consumeLogRecord(ConsumerRecord<String, LogDtoInput> logRecord) {

        LogDtoInput logDtoInput = logRecord.value();
        log.info("Log received {} from kafka topic: ", logDtoInput);
        Log log = logMapper.mapDtoInputToEntity(logDtoInput);
        logRepository.save(log);
    }
}
