package pl.mardom92.Event.Service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mardom92.Event.Service.constans.Constants;
import pl.mardom92.Event.Service.dto.log.LogDtoInput;
import pl.mardom92.Event.Service.enums.LogMessage;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class LogProducerService {

    private final KafkaSenderService kafkaSenderService;

    public void createLog(LogMessage logMessage, long memberId) {

        LogDtoInput log = LogDtoInput.builder()
                .message(
                        LogMessage.getFullLogMessage(logMessage, memberId)
                )
                .systemSource(Constants.SYSTEM_SOURCE)
                .logDate(LocalDateTime.now())
                .userId(null)//TODO: dodać id użytkownika
                .eventId(null)//TODO: dodać id wydarzenia
                .participationId(null)//TODO: dodać id udziału
                .build();

        kafkaSenderService.sendLogOnKafkaTopic(log);
    }
}
