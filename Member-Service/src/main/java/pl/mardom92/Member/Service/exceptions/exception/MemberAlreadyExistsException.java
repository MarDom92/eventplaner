package pl.mardom92.Member.Service.exceptions.exception;

public class MemberAlreadyExistsException extends RuntimeException {

    public MemberAlreadyExistsException(String message) {

        super(message);
    }
}
