package pl.mardom92.Event.Service.utils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ListManipulator {

    private ListManipulator() {

        throw new AssertionError("ListManipulator class should not be instantiated.");
    }

    private static final String COMMA = ",";

    public static <T> String getSeparatedList(Set<T> list) {

        return list.stream()
                .map(Object::toString)
                .collect(Collectors.joining(COMMA));
    }

    public static <T> String getSeparatedList(List<T> list) {

        return list.stream()
                .map(Object::toString)
                .collect(Collectors.joining(COMMA));
    }
}
