package pl.mardom92.Event.Service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mardom92.Event.Service.model.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}
