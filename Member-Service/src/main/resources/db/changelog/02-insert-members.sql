-- changeset mardom92:members_2
-- Dodawanie danych do tabeli members
INSERT INTO members (username, password, email, phone, role)
VALUES
    ('member1', 'password1', 'member1@example.com', '123456789', 'USER'),
    ('member2', 'password2', 'member2@example.com', '987654321', 'USER'),
    ('member3', 'password3', 'member3@example.com', '555555555', 'USER'),
    ('member4', 'password4', 'member4@example.com', '111111111', 'USER'),
    ('member5', 'password5', 'member5@example.com', '999999999', 'USER'),
    ('admin1', 'adminpass1', 'admin1@example.com', '777777777', 'ADMIN'),
    ('admin2', 'adminpass2', 'admin2@example.com', '888888888', 'ADMIN'),
    ('member6', 'password6', 'member6@example.com', '666666666', 'USER'),
    ('member7', 'password7', 'member7@example.com', '444444444', 'USER'),
    ('member8', 'password8', 'member8@example.com', '222222222', 'USER');