package pl.mardom92.Event.Service.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.mardom92.Event.Service.constans.Urls;
import pl.mardom92.Event.Service.dto.event.EventDtoInput;
import pl.mardom92.Event.Service.dto.event.EventDtoOutput;
import pl.mardom92.Event.Service.dto.event.EventWithIdDtoOutput;
import pl.mardom92.Event.Service.dto.member.MemberDtoInput;
import pl.mardom92.Event.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Event.Service.enums.EventStatus;
import pl.mardom92.Event.Service.enums.LogMessage;
import pl.mardom92.Event.Service.enums.Role;
import pl.mardom92.Event.Service.exceptions.exception.EventCreationFailedException;
import pl.mardom92.Event.Service.exceptions.exception.EventDetailsNotFoundException;
import pl.mardom92.Event.Service.exceptions.exception.EventIllegalArgumentException;
import pl.mardom92.Event.Service.exceptions.exception.EventNotFoundException;
import pl.mardom92.Event.Service.exceptions.exception.EventNullIdReplacementException;
import pl.mardom92.Event.Service.exceptions.exception.MemberCreationFailedException;
import pl.mardom92.Event.Service.mapper.EventMapper;
import pl.mardom92.Event.Service.model.Event;
import pl.mardom92.Event.Service.repository.EventRepository;
import pl.mardom92.Event.Service.utils.ListManipulator;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventService {

    private static final String EVENT_NOT_FOUND_WITH_ID = "Event not found with id ";

    private static final EventMapper eventMapper = EventMapper.INSTANCE;
    private final LogProducerService logProducerService;
    private final EventRepository eventRepository;
    private final RestTemplate restTemplate;

    public List<EventDtoOutput> getAllEvents() {

        return eventRepository.findAll()
                .stream()
                .map(this::mapEventEntityToEventDtoWithMembers)
                .toList();
    }

    private EventDtoOutput mapEventEntityToEventDtoWithMembers(Event event) {

        MemberDtoOutput organizerOfEvent = getOrganizerOfEvent(event);
        List<MemberDtoOutput> membersOfEvent = getMembersOfEvent(event);

        if (Objects.isNull(organizerOfEvent) || Objects.isNull(membersOfEvent)) {
            throw new EventDetailsNotFoundException("Organizer or members information is null");
        }

        return eventMapper.mapEventToDtoOutput(event, organizerOfEvent, membersOfEvent);
    }

    private MemberDtoOutput getOrganizerOfEvent(Event event) {

        try {
            Long organizerId = event.getOrganizerId();
            return getOrganizerById(organizerId);
        } catch (EventNotFoundException e) {
            throw new EventDetailsNotFoundException("Organizer not found with this id");
        }
    }

    private List<MemberDtoOutput> getMembersOfEvent(Event event) {

        try {
            Set<Long> membersIds = event.getMembersIds();
            return getMembersByIds(membersIds);
        } catch (EventNotFoundException e) {
            throw new EventDetailsNotFoundException("Members ids list not found");
        }
    }

    private MemberDtoOutput getOrganizerById(Long organizerId) {

        String url = Urls.MEMBER_BASE_URL + "/" + organizerId;

        try {
            ResponseEntity<MemberDtoOutput> organizer = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    MemberDtoOutput.class);

            if (organizer.getStatusCode().is2xxSuccessful()) {
                return organizer.getBody();
            } else {
                throw new RestClientException("Failed to retrieve organizer " + organizer.getStatusCode());
            }
        } catch (HttpClientErrorException.NotFound e) {
            throw new EventNotFoundException("Organizer not found with id " + organizerId);
        }
    }

    private List<MemberDtoOutput> getMembersByIds(Set<Long> membersIds) {

        try {
            URI url = UriComponentsBuilder.fromUriString(Urls.MEMBERS_BY_IDS_URL)
                    .pathSegment(ListManipulator.getSeparatedList(membersIds))
                    .build()
                    .toUri();

            ResponseEntity<List<MemberDtoOutput>> membersResultList = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return membersResultList.getBody();
        } catch (HttpClientErrorException.NotFound e) {
            throw new EventDetailsNotFoundException("Members of event not found with ids " + membersIds);
        }
    }

    public List<EventWithIdDtoOutput> getAllEventsWithIds() {

        return eventRepository.findAll()
                .stream()
                .map(this::mapEventEntityToEventWithIdDtoWithMembers)
                .toList();
    }

    private EventWithIdDtoOutput mapEventEntityToEventWithIdDtoWithMembers(Event event) {

        MemberDtoOutput organizerOfEvent = getOrganizerOfEvent(event);
        List<MemberDtoOutput> membersOfEvent = getMembersOfEvent(event);

        if (Objects.isNull(organizerOfEvent) || Objects.isNull(membersOfEvent)) {
            throw new EventDetailsNotFoundException("Organizer or members information is null");
        }

        return eventMapper.mapEventToWithIdDtoOutput(event, organizerOfEvent, membersOfEvent);
    }

    public EventDtoOutput getEventById(Long eventId) {

        Optional<Event> optionalEvent = eventRepository.findById(eventId);

        if (optionalEvent.isEmpty()) {
            throw new EventNotFoundException(EVENT_NOT_FOUND_WITH_ID + eventId);
        }

        Event event = optionalEvent.get();

        MemberDtoOutput organizer = getOrganizerOfEvent(event);
        List<MemberDtoOutput> members = getMembersOfEvent(event);

        return eventMapper.mapEventToDtoOutput(event, organizer, members);
    }

    @Transactional
    public EventDtoOutput createEvent(EventDtoInput eventDtoInput) {

        try {
            EventStatus eventStatus = eventDtoInput.getEventStatus();
            boolean isEventStatusCorrect = isCorrectEventStatusValue(eventStatus);

            String organizerUsername = eventDtoInput.getOrganizer().getUsername();
            Long organizerId = getOrganizerIdByUsername(organizerUsername);

            List<MemberDtoOutput> members = eventDtoInput.getMembers();
            List<String> usernames = getUsernamesFromMembers(members);
            Set<Long> allMembersIds = getAllMembersIds(usernames);

            Event event = eventMapper.mapDtoInputToEvent(eventDtoInput);
            if (!isEventStatusCorrect) {
                event.setEventStatus(EventStatus.PLANNED);
            }
            event.setOrganizerId(organizerId);
            event.setMembersIds(allMembersIds);
            Event saveEvent = eventRepository.save(event);
            logProducerService.createLog(LogMessage.ADD_EVENT, saveEvent.getId());
            return mapEventEntityToEventDtoWithMembers(event);
        } catch (Exception e) {
            throw new EventCreationFailedException("Error while creating the event");
        }
    }

    private boolean isCorrectEventStatusValue(EventStatus eventStatus) {

        return Arrays.asList(EventStatus.values()).contains(eventStatus);
    }

    private Long getOrganizerIdByUsername(String organizerUsername) {

        String url = Urls.GET_MEMBER_ID_URL + "/" + organizerUsername;

        try {
            ResponseEntity<Long> organizerId = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    Long.class);

            if (organizerId.getStatusCode().is2xxSuccessful()) {
                return organizerId.getBody();
            } else {
                throw new RestClientException("Failed to retrieve organizer id " + organizerId.getStatusCode());
            }
        } catch (HttpClientErrorException.NotFound e) {
            throw new EventNotFoundException("Organizer not found with username " + organizerUsername);
        }
    }

    private List<String> getUsernamesFromMembers(List<MemberDtoOutput> members) {

        if (Objects.isNull(members) || members.isEmpty()) {
            throw new EventIllegalArgumentException("Members list cannot be null or empty");
        }

        return members.stream()
                .map(MemberDtoOutput::getUsername)
                .toList();
    }

    private Set<Long> getAllMembersIds(List<String> usernames) {

        try {
            Map<String, Long> idsByUsernamesInMap = findMembersIdsByUsernames(usernames);
            addNewMembers(usernames, idsByUsernamesInMap);
            replaceNullIdsByMembersUsernames(idsByUsernamesInMap);
            return getAllIdsOfMembers(idsByUsernamesInMap);
        } catch (NullPointerException e) {
            throw new EventNullIdReplacementException("Unable to replace null ids for usernames " + ListManipulator.getSeparatedList(usernames));
        }
    }

    private Map<String, Long> findMembersIdsByUsernames(List<String> usernames) {

        try {
            URI url = UriComponentsBuilder.fromUriString(Urls.GET_MEMBERS_IDS_URL)
                    .pathSegment(ListManipulator.getSeparatedList(new HashSet<>(usernames)))
                    .build()
                    .toUri();

            ResponseEntity<Map<String, Long>> usernamesWithIds = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return usernamesWithIds.getBody();
        } catch (HttpClientErrorException.NotFound e) {
            throw new EventDetailsNotFoundException("Members ids not found for usernames " + usernames);
        }
    }

    private void addNewMembers(List<String> usernames, Map<String, Long> idsByUsernamesInMap) {

        usernames.stream()
                .filter(username -> Objects.isNull(idsByUsernamesInMap.get(username)))
                .forEach(this::addNewMember);
    }

    private void replaceNullIdsByMembersUsernames(Map<String, Long> idsByUsernamesInMap) {

        for (Map.Entry<String, Long> entry : idsByUsernamesInMap.entrySet()) {
            String username = entry.getKey();
            Long memberId = entry.getValue();

            if (Objects.isNull(memberId)) {
                Long newMemberId = getOrganizerIdByUsername(username);
                idsByUsernamesInMap.put(username, newMemberId);
            }
        }
    }

    private Set<Long> getAllIdsOfMembers(Map<String, Long> idsByUsernamesInMap) {

        return idsByUsernamesInMap.values().stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    private void addNewMember(String username) {

        try {
            //TODO: na razie zahardkodowane, trzeba będzie wydzielić cześć danych do mikroserwisu User-service
            MemberDtoInput memberDtoInput = MemberDtoInput.builder()
                    .username(username)
                    .password(RandomStringUtils.randomAlphanumeric(8))
                    .email(RandomStringUtils.randomAlphanumeric(8) + "@example.com")
                    .phone(RandomStringUtils.randomNumeric(9))
                    .role(Role.USER)
                    .build();

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/json");
            HttpEntity<MemberDtoInput> requestEntity = new HttpEntity<>(memberDtoInput, headers);

            String url = Urls.MEMBER_BASE_URL;
            restTemplate.exchange(url, HttpMethod.POST, requestEntity, MemberDtoOutput.class);
        } catch (HttpClientErrorException.BadRequest e) {
            throw new MemberCreationFailedException("Failed to create member with username " + username);
        }
    }

    @Transactional
    public EventDtoOutput updateEvent(Long id, EventDtoInput eventDtoInput) {

        Event existingEvent = eventRepository.findById(id)
                .orElseThrow(() -> new EventNotFoundException(EVENT_NOT_FOUND_WITH_ID + id));

        String organizerUsername = eventDtoInput.getOrganizer().getUsername();
        Long organizerId = getOrganizerIdByUsername(organizerUsername);

        List<MemberDtoOutput> members = eventDtoInput.getMembers();
        List<String> usernames = getUsernamesFromMembers(members);
        Set<Long> allMembersIds = getAllMembersIds(usernames);

        Event updatedEvent = eventMapper.mapDtoInputToEvent(eventDtoInput);
        updatedEvent.setId(existingEvent.getId());
        updatedEvent.setOrganizerId(organizerId);
        updatedEvent.setMembersIds(allMembersIds);
        eventRepository.save(updatedEvent);
        logProducerService.createLog(LogMessage.EDIT_EVENT, updatedEvent.getId());
        return mapEventEntityToEventDtoWithMembers(updatedEvent);
    }

    @Transactional
    public void deleteEvent(Long id) {

        Event existingEvent = eventRepository.findById(id)
                .orElseThrow(() -> new EventNotFoundException(EVENT_NOT_FOUND_WITH_ID + id));

        logProducerService.createLog(LogMessage.REMOVE_EVENT, id);
        eventRepository.delete(existingEvent);
    }
}
