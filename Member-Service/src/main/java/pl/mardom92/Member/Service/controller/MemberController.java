package pl.mardom92.Member.Service.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mardom92.Member.Service.dto.member.MemberDtoInput;
import pl.mardom92.Member.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Member.Service.service.MemberService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/members")
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;

    @GetMapping()
    public ResponseEntity<List<MemberDtoOutput>> getAllMembers() {

        List<MemberDtoOutput> allMembers = memberService.getAllMembers();
        return ResponseEntity.ok(allMembers);
    }

    @GetMapping("/{memberId}")
    public ResponseEntity<MemberDtoOutput> getMemberById(@PathVariable Long memberId) {

        MemberDtoOutput member = memberService.getMemberById(memberId);
        return ResponseEntity.ok(member);
    }

    @GetMapping("/by-ids/{membersIds}")
    public ResponseEntity<List<MemberDtoOutput>> getMembersByIds(@PathVariable List<Long> membersIds) {

        List<MemberDtoOutput> members = memberService.getMembersByIds(membersIds);
        return ResponseEntity.ok(members);
    }

    @GetMapping("/exist/{username}")
    public ResponseEntity<Boolean> existsByUsername(@PathVariable String username) {

        Boolean exist = memberService.existsByUsername(username);
        return ResponseEntity.ok(exist);
    }

    @GetMapping("/no-exist-list/{usernames}")
    public ResponseEntity<List<String>> findNonExistingUsernames(@PathVariable List<String> usernames) {

        List<String> nonExistingUsername = memberService.findNonExistingUsernames(usernames);
        return ResponseEntity.ok(nonExistingUsername);
    }

    @GetMapping("/get-id/{username}")
    public ResponseEntity<Long> findIdByUsername(@PathVariable String username) {

        Long idByUsername = memberService.findIdByUsername(username);
        return ResponseEntity.ok(idByUsername);
    }

    @GetMapping("/get-ids/{usernames}")
    public ResponseEntity<Map<String, Long>> findIdsByUsernamesInMap(@PathVariable List<String> usernames) {

        Map<String, Long> usernameIdMapping = memberService.findIdsByUsernamesInMap(usernames);
        return ResponseEntity.ok(usernameIdMapping);
    }

    @PostMapping()
    public ResponseEntity<MemberDtoOutput> createMember(@Valid @RequestBody MemberDtoInput memberDtoInput) {

        MemberDtoOutput memberDtoOutput = memberService.createMember(memberDtoInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(memberDtoOutput);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MemberDtoOutput> updateMember(@PathVariable Long id, @Valid @RequestBody MemberDtoInput memberDtoInput) {

        MemberDtoOutput updatedMemberDtoOutput = memberService.updateMember(id, memberDtoInput);
        return ResponseEntity.ok(updatedMemberDtoOutput);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMember(@PathVariable Long id) {

        memberService.deleteMember(id);
        return ResponseEntity.noContent().build();
    }
}
