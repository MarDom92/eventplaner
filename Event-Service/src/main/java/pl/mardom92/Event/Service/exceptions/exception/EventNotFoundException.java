package pl.mardom92.Event.Service.exceptions.exception;

public class EventNotFoundException extends RuntimeException {

    public EventNotFoundException(String message) {

        super(message);
    }
}
