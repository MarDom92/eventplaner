package pl.mardom92.Participation.Service.model;

public enum
ParticipationStatus {

    PLANNED,
    CONFIRMED,
    PENDING,
    FINISHED,
    CANCELED
}
