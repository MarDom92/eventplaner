package pl.mardom92.guiservice.GUI.Service.views.events.forms;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Getter;
import pl.mardom92.guiservice.GUI.Service.dto.form.EventDetailsFormDataDto;
import pl.mardom92.guiservice.GUI.Service.enums.EventStatus;

@Getter
public class EventDetailsForm extends FormLayout {

    private final TextField titleTextField;
    private final TextArea descriptionTextArea;
    private final DateTimePicker startDateTimePicker;
    private final DateTimePicker endDateTimePicker;
    private final ComboBox<EventStatus> eventDetailsStatusComboBox;

    public EventDetailsForm() {

        titleTextField = new TextField("Title");
        descriptionTextArea = new TextArea("Description");
        startDateTimePicker = new DateTimePicker("Start date");
        endDateTimePicker = new DateTimePicker("End date");
        eventDetailsStatusComboBox = createEventDetailsStatusComboBox();

        descriptionTextArea.setHeight("32em");
        add(createFirstColumn(), createSecondColumn());
    }

    private ComboBox<EventStatus> createEventDetailsStatusComboBox() {

        ComboBox<EventStatus> eventStatusComboBox = new ComboBox<>("Status");
        eventStatusComboBox.setItems(EventStatus.values());
        eventStatusComboBox.setItemLabelGenerator(EventStatus::getValue);
        eventStatusComboBox.setValue(EventStatus.PLANNED);

        return eventStatusComboBox;
    }

    private VerticalLayout createFirstColumn() {

        VerticalLayout firstColumnLayout = new VerticalLayout();
        firstColumnLayout.setWidthFull();
        firstColumnLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
        firstColumnLayout.add(titleTextField,
                startDateTimePicker,
                endDateTimePicker,
                eventDetailsStatusComboBox);
        return firstColumnLayout;
    }

    private VerticalLayout createSecondColumn() {

        VerticalLayout secondColumnLayout = new VerticalLayout();
        secondColumnLayout.setWidthFull();
        secondColumnLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

        secondColumnLayout.add(descriptionTextArea);

        return secondColumnLayout;
    }

    public void clearForm() {

        titleTextField.clear();
        descriptionTextArea.clear();
        startDateTimePicker.clear();
        endDateTimePicker.clear();
        eventDetailsStatusComboBox.clear();
    }

    public EventDetailsFormDataDto getFormData() {

        return EventDetailsFormDataDto.builder()
                .title(titleTextField.getValue())
                .description(descriptionTextArea.getValue())
                .startDate(startDateTimePicker.getValue())
                .endDate(endDateTimePicker.getValue())
                .eventStatus(eventDetailsStatusComboBox.getValue())
                .build();
    }

    public void setFormData(EventDetailsFormDataDto formData) {

        titleTextField.setValue(formData.getTitle());
        descriptionTextArea.setValue(formData.getDescription());
        startDateTimePicker.setValue(formData.getStartDate());
        endDateTimePicker.setValue(formData.getEndDate());
        eventDetailsStatusComboBox.setValue(formData.getEventStatus());
    }
}
