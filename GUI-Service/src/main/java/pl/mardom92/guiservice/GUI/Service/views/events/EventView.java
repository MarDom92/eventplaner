package pl.mardom92.guiservice.GUI.Service.views.events;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.mardom92.guiservice.GUI.Service.constans.Constants;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventWithIdDto;
import pl.mardom92.guiservice.GUI.Service.dto.member.MemberDto;
import pl.mardom92.guiservice.GUI.Service.enums.CrudOperationType;
import pl.mardom92.guiservice.GUI.Service.service.EventService;
import pl.mardom92.guiservice.GUI.Service.views.common.DeleteConfirmDialog;
import pl.mardom92.guiservice.GUI.Service.views.common.MainLayout;

import java.util.List;
import java.util.Objects;

@Route(value = "/events", layout = MainLayout.class)
@PageTitle("Events")
public class EventView extends VerticalLayout {

    private final transient EventService eventService;

    private final EventFormDialog eventFormDialog;
    private final HorizontalLayout horizontalLayout;
    private Grid<EventWithIdDto> eventsGrid;
    private EventWithIdDto selectedEvent;

    private Button addEventButton;
    private Button editEventButton;
    private Button removeEventButton;

    public EventView(EventService eventService) {

        this.eventService = eventService;

        eventsGrid = getEventsGrid();

        eventFormDialog = new EventFormDialog(eventService, eventsGrid);
        horizontalLayout = getCrudButtons(eventFormDialog);

        setSizeFull();
        add(horizontalLayout, eventsGrid);

        eventsGrid.addSelectionListener(this::onGridSelectionChange);
    }

    private HorizontalLayout getCrudButtons(EventFormDialog eventFormDialog) {

        addEventButton = createAddEventButton(eventFormDialog);
        editEventButton = createEditEventButton(eventFormDialog);
        removeEventButton = createRemoveEventButton();

        return new HorizontalLayout(
                addEventButton,
                editEventButton,
                removeEventButton
        );
    }

    private Grid<EventWithIdDto> getEventsGrid() {

        List<EventWithIdDto> eventDtos = eventService.getAllEvents().join();

        eventsGrid = new Grid<>(EventWithIdDto.class, false);
        eventsGrid.addColumn(EventWithIdDto::getId).setHeader("Id").setVisible(false);
        eventsGrid.addColumn(EventWithIdDto::getTitle).setHeader("Title").setFlexGrow(3);
        eventsGrid.addColumn(EventWithIdDto::getDescription).setHeader("Description").setFlexGrow(3);
        eventsGrid.addColumn(EventWithIdDto::getStartDate).setHeader("Start Date");
        eventsGrid.addColumn(EventWithIdDto::getEndDate).setHeader("End Date");
        eventsGrid.addColumn(EventWithIdDto::getEventStatus).setHeader("Status");
        eventsGrid.addComponentColumn(EventView::getSpanWithOrganizer).setHeader("Organizer").setFlexGrow(2);
        eventsGrid.addComponentColumn(this::getSpanWithNumberOfMembers).setHeader("Number of members");
        eventsGrid.setItems(eventDtos);

        return eventsGrid;
    }

    private void onGridSelectionChange(SelectionEvent<Grid<EventWithIdDto>, EventWithIdDto> event) {

        selectedEvent = event.getFirstSelectedItem().orElse(null);

        addEventButton.setEnabled(!Objects.nonNull(selectedEvent));
        editEventButton.setEnabled(Objects.nonNull(selectedEvent));
        removeEventButton.setEnabled(Objects.nonNull(selectedEvent));
    }

    private Button createAddEventButton(EventFormDialog eventFormDialog) {

        return new Button(Constants.ADD_EVENT, e -> {
            eventFormDialog.setHeaderTitle(Constants.ADD_EVENT);
            eventFormDialog.setOperationType(CrudOperationType.ADD);
            eventFormDialog.open();
        });
    }

    private Button createEditEventButton(EventFormDialog eventFormDialog) {

        Button editButton = new Button(Constants.EDIT_EVENT, e -> {
            eventFormDialog.setHeaderTitle(Constants.EDIT_EVENT);
            eventFormDialog.setOperationType(CrudOperationType.EDIT);
            eventFormDialog.setEventDtoValuesToForm(selectedEvent);
            eventFormDialog.setEventId(selectedEvent.getId());
            eventFormDialog.open();
        });
        editButton.setEnabled(false);

        return editButton;
    }

    private Button createRemoveEventButton() {

        Button removeButton = new Button(Constants.REMOVE_EVENT, e -> {
            DeleteConfirmDialog deleteConfirmDialog = new DeleteConfirmDialog(selectedEvent.getId(), eventService, eventsGrid);
            deleteConfirmDialog.open();
        });
        removeButton.setEnabled(false);

        return removeButton;
    }

    private static Span getSpanWithOrganizer(EventWithIdDto eventDto) {

        MemberDto organizer = eventDto.getOrganizer();

        if (Objects.nonNull(organizer)) {
            return new Span(organizer.getUsername());
        } else {
            return new Span();
        }
    }

    private Span getSpanWithNumberOfMembers(EventWithIdDto eventDto) {

        int numberOfMembers = eventDto.getMembers().size();
        return new Span(String.valueOf(numberOfMembers));
    }
}
