package pl.mardom92.Member.Service.exceptions.exception;

public class MemberUpdateFailedException extends RuntimeException {

    public MemberUpdateFailedException(String message) {

        super(message);
    }
}
