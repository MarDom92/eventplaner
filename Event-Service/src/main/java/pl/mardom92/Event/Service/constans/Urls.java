package pl.mardom92.Event.Service.constans;

public class Urls {

    private Urls() {

        throw new AssertionError("Urls class should not be instantiated.");
    }

    public static final String MEMBER_BASE_URL = "http://localhost:8080/members";
    public static final String MEMBERS_BY_IDS_URL = MEMBER_BASE_URL + "/by-ids";
    public static final String GET_MEMBER_ID_URL = MEMBER_BASE_URL + "/get-id";
    public static final String GET_MEMBERS_IDS_URL = MEMBER_BASE_URL + "/get-ids";
}
