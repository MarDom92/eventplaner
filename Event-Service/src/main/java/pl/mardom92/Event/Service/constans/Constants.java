package pl.mardom92.Event.Service.constans;

public final class Constants {

    private Constants() {

        throw new AssertionError("Constants class should not be instantiated.");
    }

    public static final String SYSTEM_SOURCE = "EVENT_SERVICE";
}
