package pl.mardom92.guiservice.GUI.Service.enums;

public enum CrudOperationType {
    ADD,
    EDIT,
    REMOVE
}
