package pl.mardom92.Event.Service.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ErrorResponse {

    private int statusCode;
    private String error;
    private String message;
}

