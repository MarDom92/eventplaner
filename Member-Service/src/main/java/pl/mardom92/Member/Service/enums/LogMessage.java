package pl.mardom92.Member.Service.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum LogMessage {

    ADD_MEMBER("Member added"),
    EDIT_MEMBER("Member edited"),
    REMOVE_MEMBER("Member removed");

    private final String value;

    public static String getFullLogMessage(LogMessage logMessage, long id) {

        return logMessage.value + " with id " + id;
    }
}
