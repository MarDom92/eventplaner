package pl.mardom92.Participation.Service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ParticipationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParticipationServiceApplication.class, args);
	}

}
