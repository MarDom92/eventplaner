package pl.mardom92.Event.Service.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import pl.mardom92.Event.Service.dto.event.EventDtoInput;
import pl.mardom92.Event.Service.dto.event.EventDtoOutput;
import pl.mardom92.Event.Service.dto.event.EventWithIdDtoOutput;
import pl.mardom92.Event.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Event.Service.model.Event;

import java.util.List;

@Mapper
public interface EventMapper {

    EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);

    @Mapping(source = "title", target = "title")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "startDate", target = "startDate")
    @Mapping(source = "endDate", target = "endDate")
    @Mapping(source = "eventStatus", target = "eventStatus")
    Event mapDtoInputToEvent(EventDtoInput eventDtoInput);

    @BeanMapping(qualifiedByName = "mapEventToDtoOutput")
    @Mapping(source = "title", target = "title")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "startDate", target = "startDate")
    @Mapping(source = "endDate", target = "endDate")
    @Mapping(source = "eventStatus", target = "eventStatus")
    EventDtoOutput mapEventToDtoOutput(Event event, @Context MemberDtoOutput organizer, @Context List<MemberDtoOutput> members);

    @Named("mapEventToDtoOutput")
    @AfterMapping
    default void mapOrganizerInDtoOutput(@MappingTarget EventDtoOutput eventDtoOutput, @Context MemberDtoOutput organizer) {

        eventDtoOutput.setOrganizer(organizer);
    }

    @Named("mapEventToDtoOutput")
    @AfterMapping
    default void mapMembersInDtoOutput(@MappingTarget EventDtoOutput eventDtoOutput, @Context List<MemberDtoOutput> members) {

        eventDtoOutput.setMembers(members);
    }

    @BeanMapping(qualifiedByName = "mapEventToWithIdDtoOutput")
    @Mapping(source = "id", target = "id")
    @Mapping(source = "title", target = "title")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "startDate", target = "startDate")
    @Mapping(source = "endDate", target = "endDate")
    @Mapping(source = "eventStatus", target = "eventStatus")
    EventWithIdDtoOutput mapEventToWithIdDtoOutput(Event event, @Context MemberDtoOutput organizer, @Context List<MemberDtoOutput> members);

    @Named("mapEventToWithIdDtoOutput")
    @AfterMapping
    default void mapOrganizerInDtoOutput(@MappingTarget EventWithIdDtoOutput eventWithIdDtoOutput, @Context MemberDtoOutput organizer) {

        eventWithIdDtoOutput.setOrganizer(organizer);
    }

    @Named("mapEventToWithIdDtoOutput")
    @AfterMapping
    default void mapMembersInDtoOutput(@MappingTarget EventWithIdDtoOutput eventWithIdDtoOutput, @Context List<MemberDtoOutput> members) {

        eventWithIdDtoOutput.setMembers(members);
    }
}
