package pl.mardom92.Member.Service.exceptions.exception;

public class DuplicateMemberIdException extends RuntimeException {

    public DuplicateMemberIdException(String message) {

        super(message);
    }
}
