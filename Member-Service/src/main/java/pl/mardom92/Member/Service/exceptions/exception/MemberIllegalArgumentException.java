package pl.mardom92.Member.Service.exceptions.exception;

public class MemberIllegalArgumentException extends RuntimeException {

    public MemberIllegalArgumentException(String message) {

        super(message);
    }
}
