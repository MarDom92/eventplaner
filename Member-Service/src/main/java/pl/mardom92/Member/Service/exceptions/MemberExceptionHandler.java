package pl.mardom92.Member.Service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.mardom92.Member.Service.exceptions.exception.DuplicateMemberIdException;
import pl.mardom92.Member.Service.exceptions.exception.MemberAlreadyExistsException;
import pl.mardom92.Member.Service.exceptions.exception.MemberCreationFailedException;
import pl.mardom92.Member.Service.exceptions.exception.MemberIllegalArgumentException;
import pl.mardom92.Member.Service.exceptions.exception.MemberNotFoundException;
import pl.mardom92.Member.Service.exceptions.exception.MemberUpdateFailedException;

@ControllerAdvice
public class MemberExceptionHandler {

    @ExceptionHandler(MemberNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleMemberNotFoundException(MemberNotFoundException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Not Found", e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(MemberIllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleMemberIllegalArgumentException(MemberIllegalArgumentException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Invalid member argument", e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(DuplicateMemberIdException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateMemberIdException(DuplicateMemberIdException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Bad Request", e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(MemberAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handleMemberAlreadyExistsException(MemberAlreadyExistsException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.CONFLICT.value(), "Conflict", e.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
    }

    @ExceptionHandler(MemberCreationFailedException.class)
    public ResponseEntity<ErrorResponse> handleMemberCreationException(MemberCreationFailedException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Member Creation Failed", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(MemberUpdateFailedException.class)
    public ResponseEntity<ErrorResponse> handleMemberUpdateException(MemberUpdateFailedException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Member Update Failed", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }
}
