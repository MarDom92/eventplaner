package pl.mardom92.Event.Service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.mardom92.Event.Service.exceptions.exception.EventCreationFailedException;
import pl.mardom92.Event.Service.exceptions.exception.EventDetailsNotFoundException;
import pl.mardom92.Event.Service.exceptions.exception.EventIllegalArgumentException;
import pl.mardom92.Event.Service.exceptions.exception.EventNotFoundException;
import pl.mardom92.Event.Service.exceptions.exception.EventNullIdReplacementException;
import pl.mardom92.Event.Service.exceptions.exception.EventUpdateFailedException;
import pl.mardom92.Event.Service.exceptions.exception.MemberCreationFailedException;

public class EventExceptionHandler {

    @ExceptionHandler(EventNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEntityNotFoundException(EventNotFoundException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Not Found", e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(EventDetailsNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEventDetailsNotFoundException(EventDetailsNotFoundException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Event details not found", e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(EventIllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleEventIllegalArgumentException(EventIllegalArgumentException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Invalid event argument", e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(EventNullIdReplacementException.class)
    public ResponseEntity<ErrorResponse> handleEventNullIdReplacementException(EventNullIdReplacementException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error during id replacement", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(EventCreationFailedException.class)
    public ResponseEntity<ErrorResponse> handleEventCreationException(EventCreationFailedException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Event creation failed", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(EventUpdateFailedException.class)
    public ResponseEntity<ErrorResponse> handleEventUpdateException(EventUpdateFailedException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Event update failed", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(MemberCreationFailedException.class)
    public ResponseEntity<ErrorResponse> handleMemberCreationException(MemberCreationFailedException e) {

        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Member creation failed", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }
}
