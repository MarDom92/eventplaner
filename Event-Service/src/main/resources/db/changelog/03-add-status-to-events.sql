-- changeset mardom92:events_3
-- Dodanie kolumny event_status
ALTER TABLE events
ADD COLUMN event_status VARCHAR(30);

UPDATE events
SET event_status = 'PLANNED';