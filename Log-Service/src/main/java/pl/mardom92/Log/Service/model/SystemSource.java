package pl.mardom92.Log.Service.model;

public enum SystemSource {

    EVENT_SERVICE,
    MEMBER_SERVICE,
    PARTICIPATION_SERVICE,
    WEATHER_FORECAST__SERVICE,
    PHOTO_SERVICE,
    UNKNOWN;

    public static SystemSource fromString(String value) {

        try {
            return SystemSource.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }
}
