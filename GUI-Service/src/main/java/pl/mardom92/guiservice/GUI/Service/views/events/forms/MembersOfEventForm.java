package pl.mardom92.guiservice.GUI.Service.views.events.forms;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Getter;
import pl.mardom92.guiservice.GUI.Service.dto.form.MembersOfEventFormDataDto;

import java.util.List;


@Getter
public class MembersOfEventForm extends VerticalLayout {

    private final Button addButton = new Button(new Icon(VaadinIcon.PLUS));

    public MembersOfEventForm() {

        setSpacing(false);
        setWidthFull();
        initializeAddButton();

        addButton.addClickListener(event -> addMemberRow(""));
    }

    public void initializeAddButton() {

        add(addButton);
    }

    private void addMemberRow(String username) {

        HorizontalLayout rowLayout = new HorizontalLayout();
        rowLayout.setWidthFull();
        rowLayout.setAlignItems(Alignment.BASELINE);

        TextField usernameField = new TextField("Username");
        usernameField.setWidthFull();
        usernameField.setValue(username);

        Button removeButton = new Button(new Icon(VaadinIcon.TRASH), event -> removeMemberRow(rowLayout));

        rowLayout.add(usernameField, removeButton);
        add(rowLayout);
    }

    private void removeMemberRow(HorizontalLayout memberRow) {

        remove(memberRow);
    }

    public MembersOfEventFormDataDto getFormData() {

        List<Component> components = getChildren().toList();

        MembersOfEventFormDataDto formData = new MembersOfEventFormDataDto();
        for (Component component : components) {
            if (component instanceof HorizontalLayout) {
                TextField usernameField = (TextField) ((HorizontalLayout) component).getComponentAt(0);
                formData.getUsernames().add(usernameField.getValue());
            }
        }

        return formData;
    }

    public void setFormData(MembersOfEventFormDataDto formData) {

        removeAll();
        initializeAddButton();
        formData.getUsernames().forEach(this::addMemberRow);
    }
}