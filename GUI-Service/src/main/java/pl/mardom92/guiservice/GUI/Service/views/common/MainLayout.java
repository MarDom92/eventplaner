package pl.mardom92.guiservice.GUI.Service.views.common;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.theme.lumo.LumoUtility;
import pl.mardom92.guiservice.GUI.Service.views.events.EventView;

import java.util.List;

public class MainLayout extends AppLayout {

    public MainLayout() {

        DrawerToggle toggle = new DrawerToggle();
        H1 title = new H1("Event Planner");

        SideNav nav = new SideNav();
        SideNavItem addEventLink = new SideNavItem("Events", EventView.class, VaadinIcon.USER.create());
        nav.addItem(addEventLink);

        Scroller scroller = new Scroller(nav);
        scroller.setClassName(LumoUtility.Padding.MEDIUM);

        addToDrawer(scroller);
        addToNavbar(toggle, title);
    }

    public void addComponentToDrawer(List<Component> components) {

        components.forEach(this::addToDrawer);
    }

    public void addComponentToNavbar(List<Component> components) {

        components.forEach(this::addToNavbar);
    }
}
