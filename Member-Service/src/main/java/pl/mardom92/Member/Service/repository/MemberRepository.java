package pl.mardom92.Member.Service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mardom92.Member.Service.model.Member;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

    boolean existsByUsername(String username);

    @Query("SELECT m.id FROM Member m WHERE m.username = :username")
    Long findIdByUsername(@Param("username") String username);

    @Query("SELECT username FROM Member u WHERE username IN :usernames")
    List<String> findExistingUsernames(@Param("usernames") List<String> usernames);

    default Map<String, Long> findIdsByUsernamesInMap(List<String> usernames) {

        List<Object[]> results = findIdsByUsernameIn(usernames);

        Map<String, Long> resultMap = results.stream()
                .collect(
                        LinkedHashMap::new,
                        (map, arr) -> map.put((String) arr[0], (Long) arr[1]),
                        Map::putAll
                );

        usernames.forEach(username -> resultMap.putIfAbsent(username, null));

        return resultMap;
    }

    @Query("SELECT m.username, m.id FROM Member m WHERE m.username IN :usernames")
    List<Object[]> findIdsByUsernameIn(@Param("usernames") List<String> usernames);
}
