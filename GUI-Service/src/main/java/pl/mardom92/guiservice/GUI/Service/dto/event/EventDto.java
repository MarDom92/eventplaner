package pl.mardom92.guiservice.GUI.Service.dto.event;

import lombok.Data;
import pl.mardom92.guiservice.GUI.Service.dto.member.MemberDto;
import pl.mardom92.guiservice.GUI.Service.enums.EventStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventDto {

    private String title;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private EventStatus eventStatus;
    private MemberDto organizer;
    private List<MemberDto> members;
}
