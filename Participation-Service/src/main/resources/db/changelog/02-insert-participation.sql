-- changeset mardom92:participation_2
-- Dodawanie danych do tabeli participation
INSERT INTO participation (members_ids, event_id, status)
VALUES
    ('{2, 3, 4}', 1, 'CONFIRMED'),
    ('{1, 3, 5}', 2, 'PENDING'),
    ('{1, 2, 4, 5}', 3, 'CONFIRMED'),
    ('{5, 6, 7}', 4, 'PENDING'),
    ('{1, 2, 3}', 5, 'CONFIRMED'),
    ('{4, 6, 8}', 6, 'CONFIRMED'),
    ('{2, 5, 7}', 7, 'PENDING'),
    ('{1, 4, 9}', 8, 'CONFIRMED'),
    ('{3, 7, 10}', 9, 'PENDING'),
    ('{1, 2, 5}', 10, 'CONFIRMED');