package pl.mardom92.Event.Service.exceptions.exception;

public class EventIllegalArgumentException extends RuntimeException {

    public EventIllegalArgumentException(String message) {

        super(message);
    }
}
