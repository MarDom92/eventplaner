package pl.mardom92.guiservice.GUI.Service.views.events;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import lombok.Getter;
import lombok.Setter;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventWithIdDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.EventDetailsFormDataDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.MembersOfEventFormDataDto;
import pl.mardom92.guiservice.GUI.Service.enums.CrudOperationType;
import pl.mardom92.guiservice.GUI.Service.mapper.EventMapper;
import pl.mardom92.guiservice.GUI.Service.service.EventService;
import pl.mardom92.guiservice.GUI.Service.views.common.ButtonsConfirmAndCancel;
import pl.mardom92.guiservice.GUI.Service.views.events.forms.EventDetailsForm;
import pl.mardom92.guiservice.GUI.Service.views.events.forms.MembersOfEventForm;

import java.util.List;

@Getter
public class EventFormDialog extends Dialog {

    private static final EventMapper eventMapper = EventMapper.INSTANCE;
    private final transient EventService eventService;
    private final Grid<EventWithIdDto> eventsGrid;

    private final VerticalLayout formLayout;
    private final EventDetailsForm eventDetailsForm;
    private final MembersOfEventForm membersOfEventForm;

    private final Tab eventDetailsTab;
    private final Tab membersOfEventTab;
    private final Tabs eventDialogTabs;

    private final ButtonsConfirmAndCancel buttonsConfirmAndCancel;
    private final VerticalLayout eventMainLayout;

    @Setter
    private CrudOperationType operationType;
    @Setter
    private Long eventId;

    public EventFormDialog(EventService eventService, Grid<EventWithIdDto> eventsGrid) {

        this.eventService = eventService;
        this.eventsGrid = eventsGrid;

        formLayout = new VerticalLayout();

        eventDetailsForm = new EventDetailsForm();
        membersOfEventForm = new MembersOfEventForm();

        eventDetailsTab = new Tab("Event details");
        membersOfEventTab = new Tab("Members of event");
        eventDialogTabs = new Tabs(eventDetailsTab, membersOfEventTab);
        buttonsConfirmAndCancel = new ButtonsConfirmAndCancel();
        eventMainLayout = createMainLayout(eventDialogTabs, formLayout, buttonsConfirmAndCancel);

        formLayout.setWidthFull();

        buttonsConfirmAndCancel.getCancelButton().addClickListener(event -> pressCancelButtonAction());
        buttonsConfirmAndCancel.getConfirmButton().addClickListener(event -> pressConfirmButtonAction());

        eventDialogTabs.addSelectedChangeListener(event ->
                updateContent(event.getSelectedTab()));

        addDialogCloseActionListener(this::handleDialogCloseAction);

        add(eventMainLayout);
        updateContent(eventDetailsTab);
    }

    private VerticalLayout createMainLayout(Tabs tabs, VerticalLayout verticalLayout, ButtonsConfirmAndCancel buttonsConfirmAndCancel) {

        VerticalLayout mainLayout = new VerticalLayout(tabs, verticalLayout, buttonsConfirmAndCancel);
        mainLayout.setSpacing(true);
        setWidth("60%");
        setHeight("95%");

        return mainLayout;
    }

    private void pressCancelButtonAction() {

        restoreFieldsOfEventFormDialog();
    }

    private void pressConfirmButtonAction() {

        EventDetailsFormDataDto eventDetailsFormDataDto = eventDetailsForm.getFormData();
        MembersOfEventFormDataDto membersOfEventFormDataDto = membersOfEventForm.getFormData();

        processEventByOperationType(eventId, eventDetailsFormDataDto, membersOfEventFormDataDto);

        restoreFieldsOfEventFormDialog();
    }

    private void updateContent(Tab selectedTab) {

        formLayout.removeAll();

        if (selectedTab.equals(eventDetailsTab)) {
            formLayout.add(eventDetailsForm);
        } else if (selectedTab.equals(membersOfEventTab)) {
            formLayout.add(membersOfEventForm);
        }
    }

    private void handleDialogCloseAction(Dialog.DialogCloseActionEvent event) {

        restoreFieldsOfEventFormDialog();
    }

    private void restoreFieldsOfEventFormDialog() {

        close();
        membersOfEventForm.removeAll();
        membersOfEventForm.initializeAddButton();
        eventDetailsForm.clearForm();
        eventDialogTabs.setSelectedTab(eventDetailsTab);
    }

    private void processEventByOperationType(Long eventId, EventDetailsFormDataDto eventDetailsFormDataDto, MembersOfEventFormDataDto membersOfEventFormDataDto) {

        switch (operationType) {
            case ADD -> eventService.addEvent(eventDetailsFormDataDto, membersOfEventFormDataDto);
            case EDIT -> eventService.updateEvent(eventId, eventDetailsFormDataDto, membersOfEventFormDataDto);
            default -> throw new UnsupportedOperationException("Unsupported operation: " + operationType);
        }

        List<EventWithIdDto> events = eventService.getAllEvents().join();
        eventsGrid.setItems(events);
    }

    public void setEventDtoValuesToForm(EventWithIdDto eventDto) {

        EventDetailsFormDataDto eventDetailsFormDataDto = eventMapper.mapEventDtoToEventFormData(eventDto);
        MembersOfEventFormDataDto membersOfEventFormDataDto = eventMapper.mapEventDtoToMembersOfEventFormData(eventDto);

        eventDetailsForm.setFormData(eventDetailsFormDataDto);
        membersOfEventForm.setFormData(membersOfEventFormDataDto);
    }
}
