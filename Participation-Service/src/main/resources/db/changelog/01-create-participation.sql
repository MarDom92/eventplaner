-- changeset mardom92:participation_1
-- Tworzenie tabeli participation
CREATE TABLE IF NOT EXISTS participation (
    id SERIAL PRIMARY KEY,
    members_ids BIGINT[],
    event_id BIGINT,
    status VARCHAR(255)
);