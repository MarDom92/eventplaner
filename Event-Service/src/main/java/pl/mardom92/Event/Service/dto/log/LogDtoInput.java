package pl.mardom92.Event.Service.dto.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDtoInput {

    private String message;
    private String systemSource;
    private LocalDateTime logDate;
    private Long userId;
    private Long eventId;
    private Long participationId;
}
