package pl.mardom92.guiservice.GUI.Service.constans;

public final class Constants {

    private Constants() {

        throw new AssertionError("Constants class should not be instantiated.");
    }

    public static final String ADD_EVENT = "Add event";
    public static final String EDIT_EVENT = "Edit event";
    public static final String REMOVE_EVENT = "Remove event";
}
