package pl.mardom92.Event.Service.dto.event;

import lombok.Data;
import pl.mardom92.Event.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Event.Service.enums.EventStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventWithIdDtoOutput {

    private Long id;
    private String title;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private EventStatus eventStatus;
    private MemberDtoOutput organizer;
    private List<MemberDtoOutput> members;
}
