package pl.mardom92.guiservice.GUI.Service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.mardom92.guiservice.GUI.Service.constans.Urls;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventDto;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventWithIdDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.EventDetailsFormDataDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.MembersOfEventFormDataDto;
import pl.mardom92.guiservice.GUI.Service.dto.member.MemberDto;
import pl.mardom92.guiservice.GUI.Service.mapper.EventMapper;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class EventService {

    private static final EventMapper eventMapper = EventMapper.INSTANCE;
    private final RestTemplate restTemplate;

    public CompletableFuture<List<EventWithIdDto>> getAllEvents() {

        String url = Urls.EVENTS_WITH_IDS_BASE_URL;
        return CompletableFuture.supplyAsync(() -> {
            ResponseEntity<List<EventWithIdDto>> events = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return events.getBody();
        });
    }

    public void addEvent(EventDetailsFormDataDto eventDetailsFormDataDto, MembersOfEventFormDataDto membersOfEventFormDataDto) {

        EventDto event = eventMapper.mapEventFormDataToEventDto(eventDetailsFormDataDto, membersOfEventFormDataDto);

        //TODO: na razie zahardkodowane, trzeba tutaj wstawiać username zalogowanego użytkownika
        MemberDto memberDto = new MemberDto();
        memberDto.setUsername("member1");
        event.setOrganizer(memberDto);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<EventDto> requestEntity = new HttpEntity<>(event, headers);

        String url = Urls.EVENT_BASE_URL;
        restTemplate.exchange(url, HttpMethod.POST, requestEntity, EventDto.class);
    }

    public void updateEvent(Long eventId, EventDetailsFormDataDto eventDetailsFormDataDto, MembersOfEventFormDataDto membersOfEventFormDataDto) {

        EventDto eventWithNewData = eventMapper.mapEventFormDataToEventDto(eventDetailsFormDataDto, membersOfEventFormDataDto);

        //TODO: na razie zahardkodowane, trzeba tutaj wstawiać username zalogowanego użytkownika
        MemberDto memberDto = new MemberDto();
        memberDto.setUsername("member4");
        eventWithNewData.setOrganizer(memberDto);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<EventDto> requestEntity = new HttpEntity<>(eventWithNewData, headers);

        String url = Urls.EVENT_BASE_URL + "/" + eventId;
        restTemplate.exchange(url, HttpMethod.PUT, requestEntity, EventDto.class);
    }

    public void removeEvent(Long eventId) {

        String url = Urls.EVENT_BASE_URL + "/" + eventId;
        restTemplate.exchange(url, HttpMethod.DELETE, null, EventDto.class);
    }
}
