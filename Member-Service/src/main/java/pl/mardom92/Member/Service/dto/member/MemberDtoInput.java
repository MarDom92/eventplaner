package pl.mardom92.Member.Service.dto.member;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import pl.mardom92.Member.Service.model.Role;

@Data
public class MemberDtoInput {

    @NotBlank(message = "Username cannot be empty")
    @Size(min = 6, message = "Username must be at least 8 characters long")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Username can only contain letters and digits")
    private String username;
    @NotBlank(message = "Password cannot be empty")
    @Size(min = 8, message = "Password must be at least 8 characters long")
    private String password;
    @NotBlank(message = "Email cannot be empty")
    @Email(message = "Invalid email format")
    private String email;
    @Pattern(regexp = "^\\d{9}$", message = "Phone must have 9 digits")
    private String phone;
    private Role role;
}
