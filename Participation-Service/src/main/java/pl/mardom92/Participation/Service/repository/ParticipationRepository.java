package pl.mardom92.Participation.Service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mardom92.Participation.Service.model.Participation;

@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Long> {

}
