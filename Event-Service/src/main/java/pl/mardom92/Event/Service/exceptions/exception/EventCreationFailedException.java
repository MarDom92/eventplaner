package pl.mardom92.Event.Service.exceptions.exception;

public class EventCreationFailedException extends RuntimeException {

    public EventCreationFailedException(String message) {

        super(message);
    }
}
