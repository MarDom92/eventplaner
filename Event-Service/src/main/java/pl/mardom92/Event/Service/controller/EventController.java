package pl.mardom92.Event.Service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mardom92.Event.Service.dto.event.EventDtoInput;
import pl.mardom92.Event.Service.dto.event.EventDtoOutput;
import pl.mardom92.Event.Service.dto.event.EventWithIdDtoOutput;
import pl.mardom92.Event.Service.service.EventService;

import java.util.List;

@RestController
@RequestMapping("/events")
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @GetMapping
    public ResponseEntity<List<EventDtoOutput>> getAllEvents() {

        List<EventDtoOutput> events = eventService.getAllEvents();
        return ResponseEntity.ok(events);
    }

    @GetMapping("/with-ids")
    public ResponseEntity<List<EventWithIdDtoOutput>> getAllEventsWithIds() {

        List<EventWithIdDtoOutput> events = eventService.getAllEventsWithIds();
        return ResponseEntity.ok(events);
    }

    @GetMapping("/{eventId}")
    public ResponseEntity<EventDtoOutput> getEventById(@PathVariable Long eventId) {

        EventDtoOutput eventDtoOutput = eventService.getEventById(eventId);
        return ResponseEntity.ok(eventDtoOutput);
    }

    @PostMapping()
    public ResponseEntity<EventDtoOutput> createEvent(@RequestBody EventDtoInput eventDtoInput) {

        EventDtoOutput event = eventService.createEvent(eventDtoInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(event);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDtoOutput> updateEvent(@PathVariable Long id, @RequestBody EventDtoInput eventDtoInput) {

        EventDtoOutput updatedEventDto = eventService.updateEvent(id, eventDtoInput);
        return ResponseEntity.status(HttpStatus.OK).body(updatedEventDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEvent(@PathVariable Long id) {

        eventService.deleteEvent(id);
        return ResponseEntity.noContent().build();
    }
}
