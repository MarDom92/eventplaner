package pl.mardom92.guiservice.GUI.Service.views.common;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import lombok.Getter;

@Getter
public class ButtonsConfirmAndCancel extends FlexLayout {

    private final Button cancelButton = new Button("Cancel");
    private final Button confirmButton = new Button("Confirm");

    public ButtonsConfirmAndCancel() {

        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        setWidthFull();
        setJustifyContentMode(FlexComponent.JustifyContentMode.END);

        add(cancelButton, confirmButton);
    }
}
