package pl.mardom92.guiservice.GUI.Service.dto.form;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MembersOfEventFormDataDto {

    @NotEmpty(message = "Usernames list cannot be empty")
    private List<String> usernames = new ArrayList<>();
}
