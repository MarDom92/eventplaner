package pl.mardom92.Event.Service.exceptions.exception;

public class EventNullIdReplacementException extends RuntimeException {

    public EventNullIdReplacementException(String message) {

        super(message);
    }
}
