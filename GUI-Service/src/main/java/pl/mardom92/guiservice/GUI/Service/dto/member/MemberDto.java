package pl.mardom92.guiservice.GUI.Service.dto.member;

import lombok.Data;

@Data
public class MemberDto {

    private String username;
}
