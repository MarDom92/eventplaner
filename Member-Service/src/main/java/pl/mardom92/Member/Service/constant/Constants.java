package pl.mardom92.Member.Service.constant;

public final class Constants {

    private Constants() {

        throw new AssertionError("Constants class should not be instantiated.");
    }

    public static final String SYSTEM_SOURCE = "MEMBER_SERVICE";
}
