package pl.mardom92.Member.Service.exceptions.exception;

public class MemberNotFoundException extends RuntimeException {

    public MemberNotFoundException(String message) {

        super(message);
    }
}
