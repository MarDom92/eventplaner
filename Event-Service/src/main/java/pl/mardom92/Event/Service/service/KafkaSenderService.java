package pl.mardom92.Event.Service.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mardom92.Event.Service.dto.log.LogDtoInput;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaSenderService {

    private final KafkaTemplate<String, LogDtoInput> kafkaTemplate;

    @Value("${spring.kafka.producer.topic}")
    private String topicName;

    @Transactional
    public void sendLogOnKafkaTopic(LogDtoInput logDtoInput) {

        log.info("Sending message {} on topic {}", logDtoInput, topicName);
        kafkaTemplate.send(topicName, logDtoInput);
    }
}
