package pl.mardom92.guiservice.GUI.Service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventDto;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventWithIdDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.EventDetailsFormDataDto;
import pl.mardom92.guiservice.GUI.Service.dto.form.MembersOfEventFormDataDto;
import pl.mardom92.guiservice.GUI.Service.dto.member.MemberDto;

import java.util.List;

@Mapper
public interface EventMapper {

    EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);

    @Mapping(source = "eventDetailsFormDataDto.title", target = "title")
    @Mapping(source = "eventDetailsFormDataDto.description", target = "description")
    @Mapping(source = "eventDetailsFormDataDto.startDate", target = "startDate")
    @Mapping(source = "eventDetailsFormDataDto.endDate", target = "endDate")
    @Mapping(source = "eventDetailsFormDataDto.eventStatus", target = "eventStatus")
    @Mapping(target = "eventDetailsFormData.organizer", ignore = true)
    @Mapping(source = "membersOfEventFormDataDto.usernames", target = "members")
    EventDto mapEventFormDataToEventDto(EventDetailsFormDataDto eventDetailsFormDataDto,
                                        MembersOfEventFormDataDto membersOfEventFormDataDto);

    @Mapping(source = "eventDetailsFormDataDto.title", target = "title")
    @Mapping(source = "eventDetailsFormDataDto.description", target = "description")
    @Mapping(source = "eventDetailsFormDataDto.startDate", target = "startDate")
    @Mapping(source = "eventDetailsFormDataDto.endDate", target = "endDate")
    @Mapping(source = "eventDetailsFormDataDto.eventStatus", target = "eventStatus")
    @Mapping(target = "eventDetailsFormData.organizer", ignore = true)
    @Mapping(source = "membersOfEventFormDataDto.usernames", target = "members")
    EventWithIdDto mapEventFormDataToEventWithIdDto(EventDetailsFormDataDto eventDetailsFormDataDto,
                                                    MembersOfEventFormDataDto membersOfEventFormDataDto);


    MemberDto mapToMemberDto(String username);

    List<MemberDto> mapToMembersDtoList(List<String> usernames);

    @Mapping(source = "title", target = "title")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "startDate", target = "startDate")
    @Mapping(source = "endDate", target = "endDate")
    @Mapping(source = "eventStatus", target = "eventStatus")
    EventDetailsFormDataDto mapEventDtoToEventFormData(EventWithIdDto eventDto);

    @Mapping(source = "members", target = "usernames", qualifiedByName = "mapMemberDtoListToStringList")
    MembersOfEventFormDataDto mapEventDtoToMembersOfEventFormData(EventWithIdDto eventDto);

    @Named("mapMemberDtoListToStringList")
    default List<String> mapMemberDtoListToStringList(List<MemberDto> members) {

        return members.stream()
                .map(this::mapMemberDtoToString)
                .toList();
    }

    @Named("mapMemberDtoToString")
    default String mapMemberDtoToString(MemberDto memberDto) {

        return memberDto.getUsername();
    }
}
