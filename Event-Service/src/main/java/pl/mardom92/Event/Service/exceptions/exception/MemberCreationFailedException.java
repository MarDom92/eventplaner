package pl.mardom92.Event.Service.exceptions.exception;

public class MemberCreationFailedException extends RuntimeException {

    public MemberCreationFailedException(String message) {

        super(message);
    }
}
