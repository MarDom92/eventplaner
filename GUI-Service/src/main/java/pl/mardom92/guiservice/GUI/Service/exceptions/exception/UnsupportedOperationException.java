package pl.mardom92.guiservice.GUI.Service.exceptions.exception;

public class UnsupportedOperationException extends RuntimeException {

    public UnsupportedOperationException(String message) {

        super(message);
    }
}
