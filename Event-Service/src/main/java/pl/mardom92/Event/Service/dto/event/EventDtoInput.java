package pl.mardom92.Event.Service.dto.event;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import pl.mardom92.Event.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Event.Service.enums.EventStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventDtoInput {

    @NotBlank(message = "Title cannot be empty")
    @Size(min = 5, max = 100, message = "Title must be between 5 and 100 characters long")
    private String title;
    @NotBlank(message = "Description cannot be empty")
    @Size(min = 8, message = "Description must be at least 8 characters long")
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private EventStatus eventStatus;
    @Valid
    private MemberDtoOutput organizer;
    @Valid
    private List<MemberDtoOutput> members;
}
