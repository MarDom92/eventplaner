-- changeset mardom92:members_1
-- Tworzenie tabeli members
CREATE TABLE IF NOT EXISTS members (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    phone VARCHAR(255) UNIQUE NOT NULL,
    role VARCHAR(50) NOT NULL,
    event_ids BIGINT[],
    participation_ids BIGINT[]
);