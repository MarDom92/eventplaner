package pl.mardom92.guiservice.GUI.Service.views;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.mardom92.guiservice.GUI.Service.views.common.MainLayout;


@Route("")
@PageTitle("Event Planer")
public class MainView extends VerticalLayout {

    public MainView() {

        add(new MainLayout());
    }
}
