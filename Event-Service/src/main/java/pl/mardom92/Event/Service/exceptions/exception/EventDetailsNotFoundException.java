package pl.mardom92.Event.Service.exceptions.exception;

public class EventDetailsNotFoundException extends RuntimeException {

    public EventDetailsNotFoundException(String message) {

        super(message);
    }
}
