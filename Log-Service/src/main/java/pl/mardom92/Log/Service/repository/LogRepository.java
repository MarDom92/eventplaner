package pl.mardom92.Log.Service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mardom92.Log.Service.model.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

}
