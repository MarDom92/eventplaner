-- changeset mardom92:logs_1
-- Tworzenie tabeli logs
CREATE TABLE IF NOT EXISTS logs (
    id SERIAL PRIMARY KEY,
    message VARCHAR(255),
    system_source VARCHAR(255),
    log_date TIMESTAMP,
    user_id BIGINT,
    event_id BIGINT,
    participation_id BIGINT
);