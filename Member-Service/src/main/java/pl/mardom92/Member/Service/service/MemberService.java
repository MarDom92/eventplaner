package pl.mardom92.Member.Service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mardom92.Member.Service.dto.member.MemberDtoInput;
import pl.mardom92.Member.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Member.Service.enums.LogMessage;
import pl.mardom92.Member.Service.exceptions.exception.DuplicateMemberIdException;
import pl.mardom92.Member.Service.exceptions.exception.MemberCreationFailedException;
import pl.mardom92.Member.Service.exceptions.exception.MemberIllegalArgumentException;
import pl.mardom92.Member.Service.exceptions.exception.MemberNotFoundException;
import pl.mardom92.Member.Service.exceptions.exception.MemberUpdateFailedException;
import pl.mardom92.Member.Service.mapper.MemberMapper;
import pl.mardom92.Member.Service.model.Member;
import pl.mardom92.Member.Service.model.Role;
import pl.mardom92.Member.Service.repository.MemberRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MemberService {

    private static final MemberMapper memberMapper = MemberMapper.INSTANCE;
    private final LogProducerService logProducerService;
    private final MemberRepository memberRepository;

    public List<MemberDtoOutput> getAllMembers() {

        List<Member> members = memberRepository.findAll();

        return members.stream()
                .map(memberMapper::mapMemberToDtoOutput)
                .toList();
    }

    public MemberDtoOutput getMemberById(Long memberId) {

        return memberRepository.findById(memberId)
                .map(memberMapper::mapMemberToDtoOutput)
                .orElseThrow(() -> new MemberNotFoundException("Member not found with id " + memberId));
    }

    public List<MemberDtoOutput> getMembersByIds(List<Long> membersIds) {

        if (Objects.isNull(membersIds) || membersIds.isEmpty()) {
            throw new MemberIllegalArgumentException("Members ids list cannot be null or empty");
        }

        Set<Long> uniqueIds = new HashSet<>(membersIds);

        if (uniqueIds.size() < membersIds.size()) {
            throw new DuplicateMemberIdException("Duplicate member ids are not allowed.");
        }

        List<Member> members = memberRepository.findAllById(membersIds);

        return members.stream()
                .map(memberMapper::mapMemberToDtoOutput)
                .toList();
    }

    public boolean existsByUsername(String username) {

        if (Objects.isNull(username)) {
            throw new MemberIllegalArgumentException("Username cannot be null");
        }

        return memberRepository.existsByUsername(username);
    }

    public List<String> findNonExistingUsernames(List<String> usernames) {

        if (Objects.isNull(usernames) || usernames.isEmpty()) {
            throw new MemberIllegalArgumentException("Usernames list cannot be null or empty");
        }
        List<String> existingUsername = memberRepository.findExistingUsernames(usernames);

        if (Objects.nonNull(existingUsername)) {
            usernames.removeAll(existingUsername);
        }

        return usernames;
    }

    public Long findIdByUsername(String username) {

        if (Objects.isNull(username)) {
            throw new MemberIllegalArgumentException("Username cannot be null");
        }

        return memberRepository.findIdByUsername(username);
    }

    public Map<String, Long> findIdsByUsernamesInMap(List<String> usernames) {

        if (Objects.isNull(usernames) || usernames.isEmpty()) {
            throw new MemberIllegalArgumentException("Usernames list cannot be null or empty");
        }

        return memberRepository.findIdsByUsernamesInMap(usernames);
    }

    @Transactional
    public MemberDtoOutput createMember(MemberDtoInput memberDtoInput) {

        Role role = memberDtoInput.getRole();
        boolean isRoleCorrect = isCorrectRoleValue(role);

        if (!isRoleCorrect) {
            throw new MemberIllegalArgumentException("Role of member is unknown");
        }

        String username = memberDtoInput.getUsername();
        boolean isMemberExist = memberRepository.existsByUsername(username);

        if (isMemberExist) {
            String newUsername = memberDtoInput.getUsername();
            throw new MemberCreationFailedException("The member with username " + newUsername + " already exists");
        } else {
            Member member = memberMapper.mapDtoInputToMember(memberDtoInput);
            Member saveMember = memberRepository.save(member);
            logProducerService.createLog(LogMessage.ADD_MEMBER, saveMember.getId());
            return memberMapper.mapMemberToDtoOutput(member);
        }
    }

    private boolean isCorrectRoleValue(Role role) {

        return Arrays.asList(Role.values()).contains(role);
    }

    @Transactional
    public MemberDtoOutput updateMember(Long id, MemberDtoInput memberDtoInput) {

        Member existingMember = memberRepository.findById(id)
                .orElseThrow(() -> new MemberNotFoundException("Member not found with id: " + id));

        String newUsername = memberDtoInput.getUsername();
        boolean newUsernameExistInDb = memberRepository.existsByUsername(newUsername);
        boolean newUsernameIsTheSameAsPrevious = newUsername.equals(existingMember.getUsername());

        if (newUsernameExistInDb && newUsernameIsTheSameAsPrevious) {
            throw new MemberUpdateFailedException("The member with username " + newUsername + " already exists");
        }

        Member updatedMember = memberMapper.mapDtoInputToMember(memberDtoInput);
        Long existingMemberId = existingMember.getId();
        updatedMember.setId(existingMemberId);
        memberRepository.save(updatedMember);
        logProducerService.createLog(LogMessage.EDIT_MEMBER, updatedMember.getId());
        return memberMapper.mapMemberToDtoOutput(updatedMember);
    }

    @Transactional
    public void deleteMember(Long id) {

        Member existingMember = memberRepository.findById(id)
                .orElseThrow(() -> new MemberNotFoundException("Member not found with id: " + id));

        logProducerService.createLog(LogMessage.REMOVE_MEMBER, id);
        memberRepository.delete(existingMember);
    }
}
