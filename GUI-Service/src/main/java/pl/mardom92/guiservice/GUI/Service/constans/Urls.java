package pl.mardom92.guiservice.GUI.Service.constans;

public class Urls {

    private Urls() {

        throw new AssertionError("Urls class should not be instantiated.");
    }

    public static final String EVENT_BASE_URL = "http://localhost:8080/events";
    public static final String EVENTS_WITH_IDS_BASE_URL = EVENT_BASE_URL + "/with-ids";
}
