package pl.mardom92.Member.Service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.mardom92.Member.Service.dto.member.MemberDtoInput;
import pl.mardom92.Member.Service.dto.member.MemberDtoOutput;
import pl.mardom92.Member.Service.model.Member;

@Mapper
public interface MemberMapper {

    MemberMapper INSTANCE = Mappers.getMapper(MemberMapper.class);

    @Mapping(source = "username", target = "username")
    MemberDtoOutput mapMemberToDtoOutput(Member member);

    @Mapping(source = "username", target = "username")
    @Mapping(source = "password", target = "password")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "role", target = "role")
    Member mapDtoInputToMember(MemberDtoInput member);
}
