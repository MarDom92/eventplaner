package pl.mardom92.Event.Service.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EventStatus {

    PLANNED("Planned"),
    CONFIRMED("Confirmed"),
    PENDING("Pending"),
    FINISHED("Finished"),
    CANCELED("Canceled");

    private final String value;
}
