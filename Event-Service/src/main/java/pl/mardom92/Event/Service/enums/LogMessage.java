package pl.mardom92.Event.Service.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum LogMessage {

    ADD_EVENT("Event added"),
    EDIT_EVENT("Event edited"),
    REMOVE_EVENT("Event removed");

    private final String value;

    public static String getFullLogMessage(LogMessage logMessage, long id) {

        return logMessage.value + " with id " + id;
    }
}
