package pl.mardom92.guiservice.GUI.Service.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MemberRole {

    USER("User"),
    ADMIN("Admin");

    private final String value;
}
