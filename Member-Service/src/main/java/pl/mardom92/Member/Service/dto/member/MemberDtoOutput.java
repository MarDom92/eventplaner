package pl.mardom92.Member.Service.dto.member;

import lombok.Data;

@Data
public class MemberDtoOutput {

    private String username;
}
