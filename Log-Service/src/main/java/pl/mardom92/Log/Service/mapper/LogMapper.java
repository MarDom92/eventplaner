package pl.mardom92.Log.Service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import pl.mardom92.Log.Service.dto.LogDtoInput;
import pl.mardom92.Log.Service.model.Log;
import pl.mardom92.Log.Service.model.SystemSource;

@Mapper
public interface LogMapper {

    LogMapper INSTANCE = Mappers.getMapper(LogMapper.class);

    @Mapping(source = "message", target = "message")
    @Mapping(source = "systemSource", target = "systemSource", qualifiedByName = "mapStringToSystemSource")
    @Mapping(source = "logDate", target = "logDate")
    @Mapping(source = "userId", target = "userId")
    @Mapping(source = "eventId", target = "eventId")
    @Mapping(source = "participationId", target = "participationId")
    Log mapDtoInputToEntity(LogDtoInput logDtoInput);

    @Named("mapStringToSystemSource")
    default SystemSource mapStringToSystemSource(String value) {

        return SystemSource.fromString(value);
    }
}
