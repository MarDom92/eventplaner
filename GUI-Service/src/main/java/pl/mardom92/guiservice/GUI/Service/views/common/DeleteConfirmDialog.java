package pl.mardom92.guiservice.GUI.Service.views.common;

import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.grid.Grid;
import pl.mardom92.guiservice.GUI.Service.dto.event.EventWithIdDto;
import pl.mardom92.guiservice.GUI.Service.service.EventService;

import java.util.List;

public class DeleteConfirmDialog extends ConfirmDialog {

    private final EventService eventService;
    private final Grid<EventWithIdDto> eventsGrid;

    public DeleteConfirmDialog(Long id, EventService eventService, Grid<EventWithIdDto> eventsGrid) {

        this.eventService = eventService;
        this.eventsGrid = eventsGrid;

        setHeader("Delete event");
        setText("Are you sure you want to permanently delete this item?");

        setCancelable(true);
        addCancelListener(event -> close());

        setConfirmText("Delete");
        setConfirmButtonTheme("error primary");
        addConfirmListener(event -> handleConfirm(id));
    }

    private void handleConfirm(Long id) {

        eventService.removeEvent(id);
        List<EventWithIdDto> events = eventService.getAllEvents().join();
        eventsGrid.setItems(events);
        close();
    }
}
