package pl.mardom92.Participation.Service;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import pl.mardom92.Participation.Service.service.ParticipationService;

@RestController
@RequiredArgsConstructor
public class ParticipationController {

    private final ParticipationService participationService;
}
