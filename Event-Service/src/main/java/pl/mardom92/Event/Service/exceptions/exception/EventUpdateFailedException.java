package pl.mardom92.Event.Service.exceptions.exception;

public class EventUpdateFailedException extends RuntimeException {

    public EventUpdateFailedException(String message) {

        super(message);
    }
}
