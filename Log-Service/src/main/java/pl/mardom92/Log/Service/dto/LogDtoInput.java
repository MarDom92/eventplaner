package pl.mardom92.Log.Service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDtoInput {

    private String message;
    private String systemSource;
    private LocalDateTime logDate;
    private Long userId;
    private Long eventId;
    private Long participationId;
}
